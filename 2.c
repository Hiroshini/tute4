#include <stdio.h>
#include <stdlib.h>
float diameter (float r);
float circumference (float r);
float area (float);

float diameter (float r)
{
 return 2*r;
}


float circumference (float r)
{
    return 2*3.14*r;
}

float area (float r)
{
    return 3.14*r*r;
}
int main()
{float R;
    printf("Enter the radius:\n");
    scanf("%f", &R);

    printf("The diameter of the circle is %f\n", diameter(R));
    printf("The circumference of the circle is %f\n", circumference(R));
    printf("The area of the circle is %f\n", area(R));
    return 0;
}
